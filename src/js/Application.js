import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }

  setEmojis(emojis) {
    this.emojis = emojis;
    let wrapper = document.getElementById("emojis");
    wrapper.innerHTML = "";
    this.emojis.forEach(e => {
      wrapper.append(this.addContent(e));
    });
  }

  addBananas() {
    this.emojis = this.emojis.map(emoji => {
      return emoji + this.banana
    });

    this.setEmojis(this.emojis);
  }

  addContent(content) {
    let paragraph = document.createElement("p");
    paragraph.textContent = content;
    return paragraph;
  }
}
